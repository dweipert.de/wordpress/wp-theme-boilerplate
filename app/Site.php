<?php

namespace Theme;

use Dweipert\WpEnqueueAssets\UsesWordPressScripts;
use Timber\Menu;
use Timber\Timber;
use Twig\Environment;
use Twig\Extension\StringLoaderExtension;
use Twig\TwigFunction;

class Site extends \Timber\Site
{
    use UsesWordPressScripts;

    /**
     * Site constructor.
     */
    public function __construct()
    {
        add_action('after_setup_theme', [$this, 'themeSupports']);
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);
        add_filter('timber/context', [$this, 'addToContext']);
        add_filter('timber/twig', [$this, 'addToTwig']);

        Timber::$dirname = 'resources/views';

        parent::__construct();
    }

    /**
     * Add Theme supports
     * after_setup_theme callback
     */
    public function themeSupports()
    {
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('html5');

        // menu
        add_theme_support('menus');
        register_nav_menus([
            'main' => 'Main',
            'footer' => 'Footer',
        ]);
    }

    /**
     * wp_enqueue_scripts callback
     */
    public function enqueueScripts()
    {
        $this->enqueueStyle('theme', 'index.css');
        $this->enqueueScript('theme', 'index.js', ['jquery'], true);
    }

    /**
     * @param array $context
     *
     * @return mixed
     */
    public function addToContext($context)
    {
        $context['site'] = $this;

        $context['menu'] = [];
        foreach (get_registered_nav_menus() as $location => $name) {
            $context['menu'][$location] = new Menu($location);
        }

        return $context;
    }

    /**
     * @param Environment $twig
     *
     * @return Environment
     */
    public function addToTwig(Environment $twig)
    {
        $twig->addExtension(new StringLoaderExtension());
        $twig->addFunction(new TwigFunction('assets_url', [$this, 'assetsUrl']));

        return $twig;
    }
}

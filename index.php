<?php

$context = \Timber\Timber::context();
$templates = ['index.twig'];

$context['posts'] = new \Timber\PostQuery();

\Timber\Timber::render($templates, $context);

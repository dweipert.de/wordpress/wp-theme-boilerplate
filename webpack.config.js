const defaultConfig = require('@wordpress/scripts/config/webpack.config');

module.exports = {
  ...defaultConfig,

  entry: {
    index: [
      './resources/js/index.js',
      './resources/css/index.scss',
    ],
  },
};
